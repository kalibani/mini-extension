import './style.scss';

type TypePropsInput = {
  label?: string;
  value?: string | number;
  type?: string;
  name?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  required?: boolean;
  mandatory?: boolean;
};

const TextField = ({
  label,
  value,
  type,
  onChange,
  mandatory,
  ...rest
} : TypePropsInput) : JSX.Element => (
  <div className="input-form">
    <label className="label">
      {label} {mandatory && <span className="mandatory-color">*</span>}
    </label>
    <input className="text-field" value={value} type={type} onChange={onChange} {...rest} />
  </div>
);

export default TextField;
