import type { ReactElement } from 'react'
import './style.scss';

type ListProps = {
  students?: Array<{
		id: string;
    className: string;
    fields: Array<{
			id: string,
			studentsName: string
		}>;
  }>
}

const List = ({
  students,
}: ListProps) : JSX.Element => (
	<>
		{
			students?.map((student) : ReactElement => (
				<div className="list" key={student.id}>
					<div className="list-field">Name</div>
					<div className="list-content">{student.className}</div>
					<div className="list-field">Students</div>
					<div>
						{
							student.fields.map((element) : ReactElement => (
								<span className="list-student-name" key={element.id}>{element.studentsName}</span>
							))
						}
					</div>
				</div>
			))
		}	
	</>
);

export default List;
