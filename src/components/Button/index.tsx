import './style.scss';

type ButtonProps = {
  children?: React.ReactChild;
  type?: "button" | "submit";
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

const Button = ({
  children,
  type,
  onClick
}: ButtonProps) : JSX.Element => (
  <button
    type={type}
    className="button"
    onClick={onClick}
  >
    {children}
  </button>
);

export default Button;
