import { Dispatch } from 'redux';
import { base } from 'API';
import { queryStringBuilder } from 'helpers';

// actions types
export const SET_STUDENTS = "student/SET_STUDENTS";
export const SET_STUDENT = "student/SET_STUDENT";
export const SET_LOADING = "student/SET_LOADING";
export const SET_ERROR = "student/SET_ERROR";

type setDataStudents = {
	type: typeof SET_STUDENTS,
	payload: Array<{}>
}

type setDataStudent = {
	type: typeof SET_STUDENT,
	payload: string
}

type setError = {
	type: typeof SET_ERROR,
	payload: Object;
}

type setLoading = {
	type: typeof SET_LOADING,
	payload: boolean;
}

export type Action = setDataStudents | setDataStudent | setError | setLoading;

// action creators
export const setStudent = (data : string) => (dispatch: Dispatch<Action>) => {
	dispatch({ type: SET_STUDENT, payload: data });
};

export const setStudents = (data : []) => (dispatch: Dispatch<Action>) => {
	dispatch({ type: SET_STUDENTS, payload: data });
};

export const postDataStudent = (payload : string) => async (dispatch: Dispatch<Action>) => {
	if (!payload) return
		try {
			dispatch({ type: SET_LOADING, payload: true });
			// fetch specific record by name
			const tableStudents = base('Students')
			const records = await tableStudents
				.select({ 
					filterByFormula: `({Name} = '${payload}')`,
					view: "Grid view",
				}).firstPage();
				
			// fetch classes by class id
			const idClasses : any = records[0].fields.Classes;
			const queryClass = queryStringBuilder(idClasses)
			const tableClasses = base('Classes')
			const classes = await tableClasses
				.select({ 
					filterByFormula: `OR(${queryClass})`,
					view: "Grid view",
				}).firstPage();

			let queryStudents : string = '';
			let arrayId : any = []; // flagging
			classes.forEach((field : any) => {
				const { fields : { Students } } = field;
				Students.forEach((ids : string) => {
					if (!arrayId.includes(ids)) {
						queryStudents += `RECORD_ID() = '${ids}',`
						arrayId.push(ids)
					}
				});
			})

			// fetch students by student id
			queryStudents = queryStudents.replace(/,\s*$/, ""); // trim last comma
			const students = await tableStudents
				.select({ 
					filterByFormula: `OR(${queryStudents})`,
					view: "Grid view",
				}).firstPage()
				
			// mapping student class with students name
			const classesStudents : Array<{}> = []
			classes.forEach((field : any) => {
				const arrayStudent : Array<{}> = []
				const { fields : { Students } } = field;
				Students.forEach((element : string) => {
					const student = students.find((el : any) => el.id === element)
					arrayStudent.push({
						id: student?.id,
						studentsName: student?.fields.Name
					})
				});
				classesStudents.push({
					id: field.id,
					className: field.fields.Name,
					fields: arrayStudent
				})
			})

			dispatch({ type: SET_STUDENTS, payload: classesStudents });
		} catch (error : any) {
			dispatch({ type: SET_ERROR, payload: error });
		} finally {
			dispatch({ type: SET_LOADING, payload: false });
		}
};
