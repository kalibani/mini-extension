import { combineReducers } from 'redux';
import Students from './students';

const reducers =  combineReducers({
  Students
});

export default reducers

export type RootState = ReturnType<typeof reducers>