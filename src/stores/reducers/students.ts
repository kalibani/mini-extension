import { 
	Action, 
	SET_STUDENTS, 
	SET_STUDENT,
	SET_LOADING
} from 'stores/actions/students';

const initialState = {
	students: [],
	student: '',
	isLoading: false
};

type studentReducer = typeof initialState;

const student = (state : studentReducer = initialState, { payload, type } : Action) => {
	switch (type) {
		case SET_STUDENTS:
			return {
				...state,
				students: payload
			};
		case SET_STUDENT:
			return {
				...state,
				student: payload
			};
		case SET_LOADING:
			return {
				...state,
				isLoading: payload
			};
		default:
			return state;
	}
};

export default student;
  