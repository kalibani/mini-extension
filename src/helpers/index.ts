export const queryStringBuilder = (data : []) => {
	let query = ''
	data.forEach((id : string, index : number) => {
		query += `RECORD_ID() = '${id}',`
	})
	return query.replace(/,\s*$/, ""); // trim last comma;
};