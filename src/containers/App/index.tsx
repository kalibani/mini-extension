// import type { ReactElement } from 'react'
import { FormEvent, ChangeEvent, MouseEvent } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import TextField from 'components/TextField';
import Button from 'components/Button';
import List from 'components/List';
import { postDataStudent, setStudent, SET_STUDENTS } from 'stores/actions/students';
import { RootState } from 'stores/reducers';

import './style.scss';

const App = () : JSX.Element => {
  const dispatch = useDispatch()
  const { student, students, isLoading } = useSelector((state: RootState | any) => state.Students)
  const onChange = (e: ChangeEvent<HTMLInputElement>) : void => {
    dispatch(setStudent(e.target.value))
  }

  const onLogin = (e: FormEvent<HTMLFormElement>) : void => {
    e.preventDefault()
    dispatch(postDataStudent(student))
    dispatch(setStudent(''))
  }

  const onLogout = (e: MouseEvent<HTMLButtonElement>) : void => {
    dispatch({ type: SET_STUDENTS, payload: []  })
  }

  return (
    <div className="App">
      <div className="container">
        <div className="wrapper-from">
          {
            students.length > 0 ? (
              <>
                <List students={students} />
                <Button type="button" onClick={onLogout} >Logout</Button>
              </>
            ) : isLoading ? (
              <div>Loading...</div>
            ) : (
              <form onSubmit={onLogin}>
                <TextField label="Student Name" type="text" name="name" value={student} onChange={onChange} />
                <Button type="submit" >Login</Button>
              </form>
            )
          }
        </div>
      </div>
    </div>
  );
}

export default App;
